<?php

return array (
  'qq_app_key' =>
  array (
    'title' => 'QQ',
    'type' => 'text',
    'value' => '',
    'placeholder' => '应用注册成功后分配的 APP ID',
  ),
  'qq_app_secret' =>
  array (
    'title' => 'QQ',
    'type' => 'text',
    'value' => '',
    'placeholder' => '应用注册成功后分配的KEY',
  ),
  'qq_app_callback' =>
      array (
          'title' => '回调地址',
          'type' => 'text',
          'value' => '',
          'placeholder' => 'QQ回调地址',
      ),
  'weixin_app_key' =>
  array (
    'title' => '微信',
    'type' => 'text',
    'value' => '',
    'placeholder' => '应用注册成功后分配的 APP ID',
  ),
  'weixin_app_secret' =>
      array (
          'title' => '微信',
          'type' => 'text',
          'value' => '',
          'placeholder' => '应用注册成功后分配的KEY',
      ),
  'weixin_app_callback' =>
      array (
          'title' => '回调地址',
          'type' => 'text',
          'value' => '',
          'placeholder' => '微信回调地址',
      ),
);
