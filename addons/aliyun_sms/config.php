<?php

return array (
  'AccessKeyId' =>
  array (
    'title' => 'AccessKeyId',
    'type' => 'text',
    'value' => '111111',
    'placeholder' => 'AccessKeyId',
  ),
  'AccessKeySecret' =>
  array (
    'title' => 'AccessKeySecret',
    'type' => 'text',
    'value' => '22222222',
    'placeholder' => 'AccessKeySecret',
  ),
  'sign' =>
  array (
    'title' => '短信签名',
    'type' => 'text',
    'value' => '333333',
    'placeholder' => '短信签名',
  )
);
