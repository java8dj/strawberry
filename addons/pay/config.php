<?php

return array (
  'appappid' => 
  array (
    'title' => '微信支付--APP APPID:',
    'type' => 'text',
    'value' => '',
  ),
  'gzhappid' => 
  array (
    'title' => '微信支付--公众号 APPID:',
    'type' => 'text',
    'value' => '',
  ),
  'xcxaapid' => 
  array (
    'title' => '微信支付--小程序 APPID:',
    'type' => 'text',
    'value' => '',
  ),
  'mch_id' => 
  array (
    'title' => '微信支付--mch_id:',
    'type' => 'text',
    'value' => '',
  ),
  'wxkey' => 
  array (
    'title' => '微信支付--wxkey:',
    'type' => 'text',
    'value' => '',
  ),
  'wx_notify_url' => 
  array (
    'title' => '微信支付--回调地址:',
    'type' => 'text',
    'value' => '',
  ),
  'zfb_app_id' => 
  array (
    'title' => '支付宝appid:',
    'type' => 'text',
    'value' => '',
  ),
  'zfb_ali_public_key' => 
  array (
    'title' => '支付宝zfb_ali_public_key:',
    'type' => 'textarea',
    'value' => '',
  ),
  'zfb_private_key' => 
  array (
    'title' => '支付宝private_key:',
    'type' => 'textarea',
    'value' => '',
  ),
  'zfb_notify_url' => 
  array (
    'title' => '支付宝回调地址:',
    'type' => 'text',
    'value' => '',
  ),
  'zfb_return_url' => 
  array (
    'title' => '支付宝跳转地址:',
    'type' => 'text',
    'value' => '',
  ),
);
